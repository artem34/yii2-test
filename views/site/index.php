<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= Yii::$app->name ?></h1>
    </div>

    <div class="body-content">
        <h2>
    <?= \yii\helpers\Html::a('<i class="glyphicon glyphicon glyphicon-play"></i> Show car models', '/models/index',[
            'class' => 'btn btn-success',

    ])
    ?>
        </h2>
    </div>
</div>
