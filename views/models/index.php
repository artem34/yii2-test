<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\Models;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Models */
/* @var $form ActiveForm */
?>
<?php $this->title = 'Models'; ?>

<div class="models-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php Modal::begin([
            'toggleButton' => [
                'label' => '<i class="glyphicon glyphicon-plus"></i> Add',
                'class' => 'btn btn-success',
            ],
            'closeButton' => [
                'label' => 'X',
                'class' => 'btn btn-danger btn-sm pull-right',
            ],
            'size' => 'modal-lg',
        ]);

        $model = new Models();

        echo $this->render('/models/create', ['model' => $model]);

        Modal::end();
        ?>
    </p>

    <p>

        <?php
        foreach ($arrModelIds as $modelId) {
            Modal::begin([
                'closeButton' => [
                    'label' => 'X',
                    'class' => 'btn btn-danger btn-sm pull-right',
                ],
                'size' => 'modal-lg',
                'id' => $modelId,
            ]);

            $model = new Models();

            echo $this->render('/models/update', ['modelId' => $modelId, 'model' => $model]);

            Modal::end();
        }
        ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'id' => 'model-grid-table',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'year',
            'description',
            [
                'attribute' => 'brand.name',
                'label' => 'Brand Name',
            ],
            [
                'attribute' => 'bodyType.name',
                'label' => 'Name of Car body',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view' => false,
                ],
                'buttons' => [
                    'update'=> function ($url, $model, $key) {
                            return Html::a('<i class="glyphicon glyphicon-pencil d-icon"></i>',
                                false,  // disable URL
                                [
                                    'title' => Yii::t('app', 'Update'),
                                    'class' => 'showModalButton update-model',
                                    'style' => 'cursor: pointer;',
                                    'data'  => [
                                        'url-submission' => $url,
                                    ],
                                    'data-id' => $key,
                                    'data-values' => [
                                        'name' => $model->name,
                                        'year' => $model->year,
                                        'description' => $model->description,
                                        'brand_id' => $model->brand_id,
                                        'body_type_id' => $model->body_type_id,
                                    ],
                                ]
                            );
                        },
                ],
            ],

        ],
    ]); ?>


    <?php Pjax::end(); ?>

</div><!-- models-index -->

<script type="text/javascript">
    var brands = {<?php foreach ($brands as $key => $brand) : ?>
        "<?=$key?>": "<?= $brand ?>",
        <?php endforeach; ?>
    }
</script>

<script type="text/javascript">
    var bodyTypes = {<?php foreach ($bodyTypes as $key => $bodyType) : ?>
        "<?=$key?>": "<?= $bodyType ?>",
        <?php endforeach; ?>
    }
</script>
