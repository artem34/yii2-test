<?php

use app\models\Brands;
use app\models\Models;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Models */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="models-form" id="<?= $modelId ?>">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name', [
            'inputOptions' => [
                'id' => 'models-name-' . $modelId,
                'class' => 'models-name form-control',
            ],
        ])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'year', [
            'inputOptions' => [
                'id' => 'models-year-' . $modelId,
                'class' => 'models-year form-control',
            ],
        ])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description', [
            'inputOptions' => [
                'id' => 'models-description-' . $modelId,
                'class' => 'models-description form-control',
            ],
        ])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'brand_id', [
            'inputOptions' => [
                'id' => 'models-brand_id-' . $modelId,
                'class' => 'models-brand_id form-control',
            ],
        ])->widget(\kartik\select2\Select2::classname(), [
            'data' => Models::toSelectArray((new Brands)),
            'options' => ['placeholder' => 'Select a brand ...'],
        ]); ?>

        <?= $form->field($model, 'body_type_id', [
            'inputOptions' => [
                'id' => 'models-body_type_id-' . $modelId,
                'class' => 'models-body_type_id form-control',
            ],
        ])->widget(\kartik\select2\Select2::classname(), [
            'data' => Models::toSelectArray((new \app\models\BodyTypes())),
            'language' => 'de',
            'options' => ['placeholder' => 'Select a body type ...'],
        ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success submit-action']) ?>
    </div>

    <?php ActiveForm::end() ?>

</div>
