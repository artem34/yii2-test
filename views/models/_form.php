<?php

use app\models\Brands;
use app\models\Models;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Models */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="models-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brand_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => Models::toSelectArray((new Brands)),
        'options' => ['placeholder' => 'Select a brand ...'],
    ]);
    ?>
    <?= $form->field($model, 'body_type_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => Models::toSelectArray((new \app\models\BodyTypes())),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a body type ...'],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success submit-action']) ?>
    </div>

    <?php ActiveForm::end() ?>

</div>
