<?php

namespace app\models;

use Yii;
use app\traits\Helper;

/**
 * This is the model class for table "models".
 *
 * @property int $id
 * @property string $name
 * @property string $year
 * @property string|null $description
 * @property int $brand_id
 * @property int $body_type_id
 *
 * @property BodyTypes $bodyType
 * @property Brands $brand
 */
class Models extends \yii\db\ActiveRecord
{
    use Helper;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'models';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'year', 'brand_id', 'body_type_id'], 'required'],
            [['brand_id', 'body_type_id', 'year'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['body_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BodyTypes::className(), 'targetAttribute' => ['body_type_id' => 'id']],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brands::className(), 'targetAttribute' => ['brand_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'year' => 'Year',
            'description' => 'Description',
            'brand_id' => 'Brand ID',
            'body_type_id' => 'Body Type ID',
        ];
    }

    /**
     * Gets query for [[BodyType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBodyType()
    {
        return $this->hasOne(BodyTypes::className(), ['id' => 'body_type_id']);
    }

    /**
     * Gets query for [[Brand]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }
}
