<?php

namespace app\models;

use app\traits\Helper;
use Yii;

/**
 * This is the model class for table "body_types".
 *
 * @property int $id
 * @property string $name
 *
 * @property Models[] $models
 */
class BodyTypes extends \yii\db\ActiveRecord
{
    use Helper;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'body_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Models]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModels()
    {
        return $this->hasMany(Models::className(), ['body_type_id' => 'id']);
    }
}
