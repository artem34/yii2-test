<?php

use yii\db\Migration;

/**
 * Class m210601_153440_main_migration
 */
class m210601_153440_main_migration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('brands', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(2048),
        ]);

        $this->createTable('body_types', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createTable('models', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'year' => $this->string(255)->notNull(),
            'description' => $this->string(255),
            'brand_id' => $this->integer()->notNull(),
            'body_type_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-models-brand_id',
            'models',
            'brand_id'
        );
        $this->addForeignKey(
            'fk-models-brand_id',
            'models',
            'brand_id',
            'brands',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-models-body_type_id',
            'models',
            'body_type_id'
        );
        $this->addForeignKey(
            'fk-models-body_type_id',
            'models',
            'body_type_id',
            'body_types',
            'id',
            'CASCADE'
        );

        Yii::$app->db->createCommand()->batchInsert('body_types', ['name'], [
            ['station wagon'],
            ['pickup'],
            ['van'],
            ['minivan'],
            ['sports car'],
            ['sedan'],
            ['cabriolet'],
            ['off-road car'],
            ['roadster'],
            ['two-door sedan'],
            ['limousine'],
            ['coupe'],
            ['crossover'],
            ['liftback'],
            ['hatchback'],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-models-body_type_id',
            'models'
        );
        $this->dropIndex(
            'idx-models-body_type_id',
            'models'
        );
        $this->dropForeignKey(
            'fk-models-brand_id',
            'models'
        );
        $this->dropIndex(
            'idx-models-brand_id',
            'models'
        );

        $this->dropTable('models');
        $this->dropTable('brands');
        $this->dropTable('body_types');
    }
}
