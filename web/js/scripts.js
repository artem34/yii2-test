jQuery(document).ready(function($) {
    $(function () {
        let modelsCreateForm = $(".models-create");
        let modelsUpdateForm = $(".models-update");

        modelsCreateForm.submit(function (event) {
            event.preventDefault(); // stopping submitting

            let form = $(this).find('form');
            let data = form.serializeArray();
            let url = '/models/create';

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: data
            })
                .done(function (response) {
                    if (response.data.success == true) {
                        $('.models-create').parents('.modal').modal('hide');
                        updateTable(data, response.data.id);
                    }
                })
                .fail(function (xhr) {
                    console.log(xhr);
                    console.log("error");
                });
        });

        modelsUpdateForm.submit(function (event) {
            event.preventDefault();

            let form = $(this).find('form');
            let data = form.serializeArray();
            let url = form.attr('action');
            let id = $(this).find('.models-form').prop('id');

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: data
            })
                .done(function (response) {
                    if (response.data.success == true) {
                        form.parents('.modal').modal('hide');
                        updateTable(data, id, false);
                    }
                })
                .fail(function (xhr) {
                    console.log(xhr);
                    console.log("error");
                });
        });
    });

    $(document).on('click', '.showModalButton', function(e) {
        e.preventDefault();
        let id = $(this).data('id');
        let modal = $('.models-update').parents('.modal#'+id);
        let values = $(this).data('values');

        modal.find('form').attr('action', "/models/update/" + id).submit();

        if (modal.data('bs.modal').isShown) {
            modal.find('#modalContent');
                // .load($(this).attr('value'));
            modal.find('#modalHeader').html('<h4>' + $(this).attr('title') + '</h4>');
        } else {
            modal.modal('show')
                .find('#modalContent');
                // .load($(this).attr('value'));
            modal.find('#modalHeader').html('<h4>' + $(this).attr('title') + '</h4>');
        }

        modal.find('.models-name').val(values.name);
        modal.find('.models-year').val(values.year);
        modal.find('.models-description').val(values.description);
        modal.find('.models-brand_id').val(values.brand_id).trigger('change');
        modal.find('.models-body_type_id').val(values.body_type_id).trigger('change');
        setTimeout(function () {
            modal.find('.form-group').each(function (key, item) {
                $(item).removeClass('has-error');
            });
            modal.find('.help-block').text('');
        }, 200);
    });

    function updateTable(data, id = 0, isCreate = true) {
        let modelGridTable = $('#model-grid-table');

        let newArr = {};
        $(data).each(function(index, obj){
            newArr[obj.name] = obj.value;
        });

        let tBody = modelGridTable.find('tbody');

        if (isCreate) {
            let totalItems = parseInt(modelGridTable.find('.summary').find('b:last-child').text());
            let newSumItems = modelGridTable.find('.summary').find('b:last-child').length > 0 ? totalItems + 1 : 1;

            modelGridTable.find('.summary').find('b:first-child').text('1-' + newSumItems);
            modelGridTable.find('.summary').find('b:last-child').text(newSumItems);

            if (tBody.find('.empty').length > 0) {
                tBody.html('');
            }

            tBody.append('<tr data-key="'+id+'">' +
                '<td>'+newSumItems+'</td>' +
                '<td>'+newArr["Models[name]"]+'</td>' +
                '<td>'+newArr["Models[year]"]+'</td>' +
                '<td>'+newArr["Models[description]"]+'</td>' +
                '<td>'+brands[newArr["Models[brand_id]"]]+'</td>' +
                '<td>'+bodyTypes[newArr["Models[body_type_id]"]]+'</td>' +
                '<td>' +
                    '<a class="showModalButton update-model" title="Update" style="cursor: pointer;" data-url-submission="/models/update/'+id+'" data-id="'+id+'" data-values="{&quot;name&quot;:&quot;'+newArr["Models[name]"]+'&quot;,&quot;year&quot;:&quot;'+newArr["Models[year]"]+'&quot;,&quot;description&quot;:&quot;'+newArr["Models[description]"]+'&quot;,&quot;brand_id&quot;:'+newArr["Models[brand_id]"]+',&quot;body_type_id&quot;:'+newArr["Models[body_type_id]"]+'}"><i class="glyphicon glyphicon-pencil d-icon"></i></a> ' +
                    '<a href="/models/delete/'+id+'" title="Delete" aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?" data-method="post"><svg aria-hidden="true" style="display:inline-block;font-size:inherit;height:1em;overflow:visible;vertical-align:-.125em;width:.875em" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M32 464a48 48 0 0048 48h288a48 48 0 0048-48V128H32zm272-256a16 16 0 0132 0v224a16 16 0 01-32 0zm-96 0a16 16 0 0132 0v224a16 16 0 01-32 0zm-96 0a16 16 0 0132 0v224a16 16 0 01-32 0zM432 32H312l-9-19a24 24 0 00-22-13H167a24 24 0 00-22 13l-9 19H16A16 16 0 000 48v32a16 16 0 0016 16h416a16 16 0 0016-16V48a16 16 0 00-16-16z"></path></svg></a>' +
                '</td>' +
            '</tr>');
        } else {
            let row = tBody.find('tr[data-key='+id+']');

            if (row.length > 0) {
                row.find('td:nth-child(2)').text(newArr["Models[name]"]);
                row.find('td:nth-child(3)').text(newArr["Models[year]"]);
                row.find('td:nth-child(4)').text(newArr["Models[description]"]);
                row.find('td:nth-child(5)').text(brands[newArr["Models[brand_id]"]]);
                row.find('td:nth-child(6)').text(bodyTypes[newArr["Models[body_type_id]"]]);
            }
        }
    }
});
