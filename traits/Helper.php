<?php

namespace app\traits;


trait Helper
{
    public static function toSelectArray($objects = null) : array
    {
        if (is_null($objects)) return [];

        $result = [];

        $data = $objects::find()->asArray()->all();

        foreach ($data as $datum) {
            $result[$datum['id']] = $datum['name'];
        }

        return $result;

    }
}